import { states, drugs } from '../config/cures.json'; // eslint-disable-line

export interface PatientsRegister {
  [key: string]: number;
}

export const validStates = states.map((state) => state.key);
export const validDrugs = drugs.map((drug) => drug.key);
export const addMissingProperty = (patientsRegister: PatientsRegister): PatientsRegister => {
  const patients = patientsRegister;
  validStates.forEach((validState: string) => {
    if (!patients[validState]) {
      patients[validState] = 0;
    }
  });

  return patients;
};
