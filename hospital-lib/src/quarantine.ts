import { rules, deadState as X } from '../config/cures.json'; // eslint-disable-line
import { Rule } from './cures.type';
import { addMissingProperty, PatientsRegister } from './patientsRegister';
import { drugsValidator, patientsRegisterValidator } from './validator';

export class Quarantine {
  /**
   *
   * @private drugs, administered drugs
   */
  private drugs: Array<string> = [];

  /**
   *
   * @private patients, initial patient admitted
   */
  private patients: PatientsRegister;

  /**
   *
   * @private curedPatients, patients state after cured
   */
  private curedPatients: PatientsRegister;

  constructor(patients: PatientsRegister) {
    patientsRegisterValidator(patients);
    this.patients = addMissingProperty(patients);
  }

  public setDrugs(administeredDrugs: Array<string>): void {
    drugsValidator(administeredDrugs);
    this.drugs = administeredDrugs;
  }

  public wait40Days(): boolean {
    this.curedPatients = {};
    // generic rules
    if (this.appliedGenericRules()) {
      return true;
    }
    // specific rule (The object is validated to prevent iteration over unexpected member)
    for (const state in this.patients) {
      // dead patients stay dead!
      if (state === X) {
        this.curedPatients.X ? (this.curedPatients.X += this.patients.X) : (this.curedPatients.X = this.patients.X);
        continue;
      }
      // get only rules applied for the patient state
      const appliedSpecificRules = rules.filter((rule) => rule.illness === state);
      if (appliedSpecificRules.length > 0) {
        // get rules matching the drugs set with the drugs configured
        const appliedSpecificDrugs = appliedSpecificRules.find((rule) => rule.drugs.filter((d) => !this.drugs.includes(d)).length === 0);
        this.appliedSpecificRules(appliedSpecificRules[0], state, appliedSpecificDrugs);
      } else {
        // prevent mis configuration between states and rules
        throw new Error(`no rule found for ${state}`);
      }
    }

    return true;
  }

  public report(): PatientsRegister {
    const report: PatientsRegister = {};
    for (const state in this.patients) {
      report[state] = this.curedPatients ? (this.curedPatients[state] ?? 0) : this.patients[state];
    }
    return report;
  }

  // --------- private methods ---------

  private appliedGenericRules(): boolean {
    // rules for all illness
    const genericRules = rules.filter((rule) => rule.illness === null);
    let mark = false;

    for (const genericRule of genericRules) {
      // Get the match drugs for each rules;
      const appliedGenericDrugs = genericRule.drugs.filter((d) => !this.drugs.includes(d)).length;
      if (appliedGenericDrugs === 0) {
        // sum the total patients concerned by the generic rule
        this.curedPatients[genericRule.result] = Object.values(this.patients).reduce((p: number, c: number) => (p + c), 0);
        // done if a generic rule has the priority (example death)
        if (genericRule.priority) {
          return true;
        }

        mark = true;
      }
    }
    // done if a generic rules is applied
    return mark;
  }

  private appliedSpecificRules(appliedSpecificRule: Rule, state: string, appliedSpecificDrugs?: Rule): void {
    const patients = this.patients[state];
    if (appliedSpecificDrugs) {
      // if no cure set, take the result of the appliedSpecificDrugs
      if (appliedSpecificDrugs.drugs.length === 0) {
        const nextState = appliedSpecificDrugs.result;
        this.curedPatients[nextState] ? (this.curedPatients[nextState] += patients) : (this.curedPatients[nextState] = patients);
        return;
      }

      // or take the current rule result (rule "only one rule can be applied")
      const nextState = appliedSpecificRule.result;
      this.curedPatients[nextState] ? (this.curedPatients[nextState] += patients) : (this.curedPatients[nextState] = patients);
      return;
    }
    // or the cure doesn't change the patient state
    this.curedPatients[state] ? (this.curedPatients[state] += patients) : (this.curedPatients[state] = patients);
  }
}
