import {Expect, Test, TestFixture} from 'alsatian';

import {drugsValidator, patientsRegisterValidator} from "./validator";

@TestFixture()
export class ValidatorTest {
    @Test()
    public validPatients() {
        Expect(patientsRegisterValidator({
            F: 1, H: 2, D: 3, T: 1, X: 0
        })).toEqual(undefined);
    }

    @Test()
    public invalidPatientsState() {
        let message = '';
        try {
            patientsRegisterValidator({
                A: 1, H: 2, D: 3, T: 1, X: 0
            })
        } catch (e) {
            message = e.message;
        } finally {
            Expect(message).toEqual('Following state are not configured: A')
        }
    }

    @Test()
    public validDrugs() {
        Expect(drugsValidator(['As', 'P'])).toEqual(undefined);
    }

    @Test()
    public invalidDrugsState() {
        let message = '';
        try {
            drugsValidator(['toto'])
        } catch (e) {
            message = e.message;
        } finally {
            Expect(message).toEqual('Following drugs are not configured: toto')
        }
    }
}
