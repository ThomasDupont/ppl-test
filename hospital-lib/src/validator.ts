import { PatientsRegister, validDrugs, validStates } from './patientsRegister'; // eslint-disable-line

export const patientsRegisterValidator = (patientsRegister?: PatientsRegister): void => {
  if (!patientsRegister) {
    throw new Error('patients register is not defined');
  }

  const diff = Object.keys(patientsRegister).filter((key) => validStates.indexOf(key) === -1);

  if (diff.length > 0) {
    throw new Error(`Following state are not configured: ${diff.join(',')}`);
  }
};

export const drugsValidator = (drugs?: Array<string>): void => {
  if (!drugs) {
    throw new Error('drugs is not defined');
  }

  const diff = drugs.filter((key) => validDrugs.indexOf(key) === -1);

  if (diff.length > 0) {
    throw new Error(`Following drugs are not configured: ${diff.join(',')}`);
  }
};
