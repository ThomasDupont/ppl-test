export type Rule = {
  priority: boolean;
  drugs: Array<string>;
  illness: string;
  result: string;
};
