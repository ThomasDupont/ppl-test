import { Component } from '@angular/core';
import { Quarantine } from 'hospital-lib';
import { PatientsRegister } from 'hospital-lib/dist/src';
import { SimulationCacheService } from './services/simulation-cache.service';
import { HospitalBeService } from './services/hospital-be.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public title = 'Hospital administration';
  public patientsRegister: PatientsRegister;
  public drugs: Array<string>;
  public report: PatientsRegister;
  public error: string;

  constructor(
    public simulationCacheService: SimulationCacheService,
    public hospitalBeService: HospitalBeService
  ) {}

  public fetchPatientsStatesAndDrugs() {
    this.hospitalBeService.fetchPatientsStates().subscribe((patients) => {
      this.patientsRegister = {};
      for (const el of patients.toString().split(',')) {
        this.patientsRegister[el] ? this.patientsRegister[el] += 1 : this.patientsRegister[el] = 1;
      }
    });

    this.hospitalBeService.fetchPatientsDrugs().subscribe((drugs) => {
      this.drugs = drugs.toString() !== '' ? drugs.toString().split(',') : [];
    });
  }

  public applyQuarantine() {
    this.error = '';
    try {
      const quarantine = new Quarantine(this.patientsRegister);
      quarantine.setDrugs(this.drugs);
      quarantine.wait40Days();
      this.report = quarantine.report();
      this.simulationCacheService.set({
        states: {...this.report},
        drugs: [...this.drugs],
        initial: {...this.patientsRegister}
      });
    } catch (e) {
      this.error = e.message;
    }

    this.patientsRegister = undefined;
    this.report = undefined;
    this.drugs = undefined;
  }
}
