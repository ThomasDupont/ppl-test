import { TestBed } from '@angular/core/testing';

import { HospitalBeService } from './hospital-be.service';
import { HttpClientModule } from '@angular/common/http';

describe('HospitalBeService', () => {
  let service: HospitalBeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
    });
    service = TestBed.inject(HospitalBeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
