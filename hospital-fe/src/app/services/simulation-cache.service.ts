import { Injectable } from '@angular/core';
import { PatientsRegister } from 'hospital-lib/dist/src';
import { SIMULATION_LIMIT } from '../config';

interface Simulation {
  drugs: Array<string>;
  initial: PatientsRegister;
  states: PatientsRegister;
}
@Injectable({
  providedIn: 'root'
})
export class SimulationCacheService {
  public simulations: Simulation[] = [];
  constructor() { }

  public set(simulation: Simulation): void {
    if (this.simulations.length === SIMULATION_LIMIT) {
      this.simulations.shift();
    }
    this.simulations.push(simulation);
  }
}
