import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {BACK_END_URL} from '../config';

@Injectable({
  providedIn: 'root'
})
export class HospitalBeService {

  constructor(private http: HttpClient) { }

  public fetchPatientsStates(): Observable<object> {
    return this.http.get(`${BACK_END_URL}/patients`).
    pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  public fetchPatientsDrugs(): Observable<object> {
    return this.http.get(`${BACK_END_URL}/drugs`).
    pipe(
      retry(3),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(`Backend returned code ${error.status}, body was: ${error.error}`);
    }
    return throwError('Something bad happened; please try again later.');
  }
}
