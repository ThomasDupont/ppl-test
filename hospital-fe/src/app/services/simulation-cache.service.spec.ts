import { TestBed } from '@angular/core/testing';

import { SimulationCacheService } from './simulation-cache.service';

describe('SimulationCacheService', () => {
  let service: SimulationCacheService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SimulationCacheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
